import { TestBed } from '@angular/core/testing';

import { ArticleService } from './article.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Article} from "../../shared/model/article";
import {of} from "rxjs";

describe('ArticleService', () => {
  let service: ArticleService;
  let httpClient: HttpClient;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ArticleService]
    });
    service = TestBed.inject(ArticleService);
    httpClient = TestBed.inject(HttpClient);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('getArticles should return articles with calculated taxes and TTC price', () => {
    const articles: Article[] = [
      { productName: 'Book', price: 10, category: 'books', isImported: false } as Article,
      { productName: 'Food', price: 20, category: 'Food', isImported: true } as Article,
    ];

    const expectedArticles: Article[] = [
      { productName: 'Book', price: 10, category: 'books', isImported: false, taxes: 1, prixUnitaireTTC: 10.1 } as Article,
      { productName: 'Food', price: 20, category: 'Food', isImported: true, taxes: 1, prixUnitaireTTC: 20.2 } as Article,
    ];

    const spy = spyOn(httpClient, 'get').and.returnValue(of(articles));

    service.getArticles().subscribe((returnedArticles) => {
      expect(returnedArticles).toEqual(expectedArticles);
    });

    expect(spy).toHaveBeenCalledWith('assets/articles.json');
  });

  it('calculerTaxes should calculate tax based on category and imported status', () => {
    const article1: Article = { productName: 'Book', price: 20, category: 'books', isImported: false } as Article;
    const article2: Article = { productName: 'Food', price: 20, category: 'Food', isImported: true } as Article;
    const article3: Article = { productName: 'Other', price: 30, category: 'electronics', isImported: false } as Article;

    const articleWithTax1 = service.calculerTaxes(article1);
    const articleWithTax2 = service.calculerTaxes(article2);
    const articleWithTax3 = service.calculerTaxes(article3);

    expect(articleWithTax1.taxes).toBe(2); // 10% tax on Books
    expect(articleWithTax2.taxes).toBe(1); // 0% tax on Food + 5% import tax
    expect(articleWithTax3.taxes).toBe(6); // 20% default tax
  });

  it('calculerMontantTTC should calculate total price including tax', () => {
    const article: Article = { productName: 'Book', price: 10, category: 'books', isImported: false, taxes: 1 } as Article;

    const articleWithTTC = service.calculerMontantTTC(article);

    expect(articleWithTTC.prixUnitaireTTC).toBe(10.1);
  });
});
