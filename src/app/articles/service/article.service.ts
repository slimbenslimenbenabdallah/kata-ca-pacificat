import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {filter, map, Observable} from "rxjs";
import {Article} from "../../shared/model/article";
import {PanierRow} from "../../shared/model/panier";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private httpClient: HttpClient) {
  }

  getArticles(): Observable<Article[]> {
    return this.httpClient
      .get<Article[]>("assets/articles.json")
      .pipe(
        map(articles => articles
          .filter(article => article.productName)),
        map(articles =>
          articles.map( article => this.calculerTaxes(article))
        ),
        map(articles =>
          articles.map( article => this.calculerMontantTTC(article))
        )
      )
  }

  /**
   * calculer taxe unitaire
   * @param article -
   */
  calculerTaxes(article : Article): Article {

    // taxe 20% par défaut
    let tauxTaxe:number = 20;

    const upperCasedCategory = article.category.toUpperCase();
    //aucun taxe appliqué
    if(["FOOD", "MEDECINE"].includes(upperCasedCategory)) {
      tauxTaxe = 0;
    }

    //taxe 10% livres
    if("BOOKS" === upperCasedCategory) {
      tauxTaxe = 10;
    }

    if(article.isImported) {
      tauxTaxe += 5;
    }

    article.taxes = (article.price / 100) * tauxTaxe;

    return article;
  }

  /**
   * calculer le Montant TTC unitaire
   * @param article -
   */
  calculerMontantTTC(article : Article): Article {
    article.prixUnitaireTTC =  article.price + (article.price * article.taxes) / 100;
    return article;
  }
}
