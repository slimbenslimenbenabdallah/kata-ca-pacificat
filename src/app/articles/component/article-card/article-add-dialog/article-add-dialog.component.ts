import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-article-add-dialog',
  templateUrl: './article-add-dialog.component.html',
  styleUrl: './article-add-dialog.component.scss'
})
export class ArticleAddDialogComponent {

  readonly DEFAULT_QUANTITY_VALUE = 1

  quantityFormControl: FormControl = new FormControl<number>(this.DEFAULT_QUANTITY_VALUE);

  @Output() quantityAjouter = new EventEmitter<number>();

  constructor(@Inject(MAT_DIALOG_DATA) public dialogData: DialogData) {}

  resetFormControl() {
    this.quantityFormControl.reset(this.DEFAULT_QUANTITY_VALUE)
  }

  ajouterArticle() {
    this.quantityAjouter.emit(this.quantityFormControl.value);

  }
}

interface DialogData {
  qteDisponible: number;
}
