import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleAddDialogComponent } from './article-add-dialog.component';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";

describe('ArticleAddDialogComponent', () => {
  let component: ArticleAddDialogComponent;
  let fixture: ComponentFixture<ArticleAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArticleAddDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset form control to default value', () => {
    component.quantityFormControl.setValue(5);
    component.resetFormControl();
    expect(component.quantityFormControl.value).toBe(component.DEFAULT_QUANTITY_VALUE);
  });

  it('should emit quantity on ajouterArticle', () => {
    spyOn(component.quantityAjouter, 'emit');
    component.quantityFormControl.setValue(3);
    component.ajouterArticle();
    expect(component.quantityAjouter.emit).toHaveBeenCalledWith(3);
  });
});
