import {Component, Input, OnInit} from '@angular/core';
import {Article} from "../../../shared/model/article";
import {PanierService} from "../../../shared/service/panier/panier.service";
import {PanierRow} from "../../../shared/model/panier";
import {MatDialog} from "@angular/material/dialog";
import {ArticleAddDialogComponent} from "./article-add-dialog/article-add-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrl: './article-card.component.scss'
})
export class ArticleCardComponent implements OnInit{
  @Input() article: Article;
  row : PanierRow | undefined;

  constructor(private panierService: PanierService,
              private dialog: MatDialog,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.row = this.panierService.getRowArticle(this.article);
  }

  get qteDisponible(): number {
    if(this.row) {
      return this.article.quantity - this.row.qte;
    }
    return this.article.quantity;
  }

  openDialog(article: Article) {
    const dialogRef = this.dialog.open(ArticleAddDialogComponent,
      {
        width: '350px',
        data: {
          qteDisponible: this.qteDisponible,
        }
      });

    dialogRef.componentInstance.quantityAjouter.subscribe(qte => {
      this.row = this.panierService.ajouterArticle(article, qte);
      this._snackBar.open( article.productName + " a été ajouté avec succès dans votre panier");
    });
  }
}
