import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleCardComponent} from './article-card.component';
import {PanierService} from "../../../shared/service/panier/panier.service";
import {of} from "rxjs";
import {PanierRow} from "../../../shared/model/panier";
import {Article} from "../../../shared/model/article";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";

describe('ArticleCardComponent', () => {
  let component: ArticleCardComponent;
  let fixture: ComponentFixture<ArticleCardComponent>;
  let article: Article;

  beforeEach(async () => {
    article = {id: 1, productName: 'Article 1', quantity: 2} as Article;
    await TestBed.configureTestingModule({
      declarations: [ArticleCardComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [{
          provide: PanierService, useValue: {
            ajouterArticle(article: Article, qte: number) {
            },
            getRowArticle: () => of(new PanierRow(article, 2))
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleCardComponent);
    component = fixture.componentInstance;
    component.article = article;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate qteDisponible correctly when row exists', () => {
    component.row = new PanierRow(article, 1)
    expect(component.qteDisponible).toBe(1);
  });

  it('should calculate qteDisponible correctly when row does not exist', () => {
    component.row = undefined;
    expect(component.qteDisponible).toBe(2);
  });

});
