import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticlesFilterComponent} from './articles-filter.component';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
import {of} from "rxjs";
import {Article} from "../../../../shared/model/article";
import {MatAutocompleteModule} from "@angular/material/autocomplete";

describe('ArticlesFilterComponent', () => {
  let component: ArticlesFilterComponent;
  let fixture: ComponentFixture<ArticlesFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArticlesFilterComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [MatAutocompleteModule]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ArticlesFilterComponent);
    component = fixture.componentInstance;
    component.articles$ = of([
      {
        "id": 1,
        "productName": "test",
        "price": 1.76,
        "quantity": 7,
        "isImported": true,
        "category": "Food"
      } as Article
    ])
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
