import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {map, Observable, tap} from "rxjs";
import {FormControl} from "@angular/forms";
import {Article} from "../../../../shared/model/article";

@Component({
  selector: 'app-articles-filter',
  templateUrl: './articles-filter.component.html',
  styleUrl: './articles-filter.component.scss'
})
export class ArticlesFilterComponent implements OnInit {
  categories$: Observable<string[]>;
  categorieFormControl: FormControl = new FormControl<string>('');

  @Input() articles$: Observable<Article[]>;
  @Output() categoryChanged = new EventEmitter<string>();

  ngOnInit(): void {
    this.categories$ = this.articles$.pipe(
      map(articles => articles.map(article => article.category)),
      map(categories => [...new Set(categories)])
    );

    //ajouter un filter sur les categorie sans modifier l'Observable
    this.categorieFormControl.valueChanges.pipe(
      tap(categorie => this.categoryChanged.emit(categorie))
    ).subscribe();
  }
}
