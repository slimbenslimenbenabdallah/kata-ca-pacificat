import {Component, OnInit} from '@angular/core';
import {ArticleService} from "../../service/article.service";
import {map, Observable, tap} from "rxjs";
import {Article} from "../../../shared/model/article";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-articles',
  templateUrl: './article-list.component.html',
  styleUrl: './article-list.component.scss'
})
export class ArticleListComponent implements OnInit {

  private _articles$: Observable<Article[]>;
  filtredArticles$: Observable<Article[]>;

  constructor(private articleService: ArticleService) {
  }

  ngOnInit(): void {
    this.filtredArticles$ = this._articles$ = this.articleService.getArticles();
  }

  onCategoryChanged(categorie: string) {
    this.filtredArticles$ = this._articles$

    if (categorie) {
      this.filtredArticles$ = this.filtredArticles$.pipe(
        map(articles => articles.filter(article => article.category === categorie))
      );
    }

  }
}
