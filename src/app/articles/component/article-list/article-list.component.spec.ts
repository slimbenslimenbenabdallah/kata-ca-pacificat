import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleListComponent } from './article-list.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {ArticleService} from "../../service/article.service";
import {HttpClientModule} from "@angular/common/http";

describe('ArticlesComponent', () => {
  let component: ArticleListComponent;
  let fixture: ComponentFixture<ArticleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArticleListComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      providers: [ArticleService],
      imports: [HttpClientModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
