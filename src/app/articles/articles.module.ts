import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ArticlesRoutingModule} from './articles-routing.module';
import {ArticleListComponent} from './component/article-list/article-list.component';
import {ArticleCardComponent} from './component/article-card/article-card.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";
import {SharedModule} from "../shared/shared.module";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { ArticleAddDialogComponent } from './component/article-card/article-add-dialog/article-add-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatIconModule} from "@angular/material/icon";
import { ArticlesFilterComponent } from './component/article-list/articles-filter/articles-filter.component';

@NgModule({
  declarations: [
    ArticleListComponent,
    ArticleCardComponent,
    ArticleAddDialogComponent,
    ArticlesFilterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatIconModule,
    ArticlesRoutingModule,
  ]
})
export class ArticlesModule {
}
