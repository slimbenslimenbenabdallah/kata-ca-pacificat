import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanierComponent } from './panier.component';
import {Article} from "../shared/model/article";
import {PanierRow} from "../shared/model/panier";
import {MatSnackBar} from "@angular/material/snack-bar";
import {PanierService} from "../shared/service/panier/panier.service";
import {of} from "rxjs";

class MatSnackBarMock {
  open(message: string) {}
}

class PanierServiceMock {
  panier$ = of({ rows: [] }); // Assuming initial panier is empty
  supprimerArticle(article: Article) {}
}
describe('PanierComponent', () => {
  let component: PanierComponent;
  let fixture: ComponentFixture<PanierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PanierComponent],
      providers: [
        { provide: MatSnackBar, useClass: MatSnackBarMock },
        { provide: PanierService, useClass: PanierServiceMock }
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call supprimerArticle on delete', () => {
    const article = { productName: 'Product', taxes: 0.1, prixUnitaireTTC: 10 } as Article;
    const panierService = TestBed.inject(PanierService);
    const matSnackBar = TestBed.inject(MatSnackBar);
    const spy = spyOn(panierService, 'supprimerArticle').and.callThrough();
    const snackBar = spyOn(matSnackBar, 'open');

    component.supprimerArticle(article);

    expect(spy).toHaveBeenCalledWith(article);
    expect(snackBar).toHaveBeenCalledWith(`${article.productName} a été supprimé avec succès de votre panier`);
  });

  it('should calculate total taxes correctly', () => {
    component.dataSource.data = [{ article: { taxes: 0.1, prixUnitaireTTC: 10 }, qte: 2 } as PanierRow];
    expect(component.calculeTotaleTaxes()).toEqual(2 * 0.1);
  });

  it('should calculate total TTC correctly', () => {
    component.dataSource.data = [{ article: { taxes: 0.1, prixUnitaireTTC: 10 }, qte: 2 } as PanierRow];
    expect(component.calculeTotaleTTC()).toEqual(2 * 10 );
  });

});
