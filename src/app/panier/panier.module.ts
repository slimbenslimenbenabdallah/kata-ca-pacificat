import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanierRoutingModule } from './panier-routing.module';
import {PanierComponent} from "./panier.component";
import {SharedModule} from "../shared/shared.module";
import {MatTableModule} from "@angular/material/table";
import {MatIcon} from "@angular/material/icon";
import {MatIconButton} from "@angular/material/button";


@NgModule({
  declarations: [
    PanierComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatTableModule,
    PanierRoutingModule,
    MatIcon,
    MatIconButton
  ]
})
export class PanierModule { }
