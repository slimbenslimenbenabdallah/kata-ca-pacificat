import {Component, OnInit} from '@angular/core';
import {PanierService} from "../shared/service/panier/panier.service";
import {PanierRow} from "../shared/model/panier";
import {MatTableDataSource} from "@angular/material/table";
import {Article} from "../shared/model/article";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrl: './panier.component.scss'
})
export class PanierComponent implements OnInit {

  displayedColumns: string[] = ["Nom", "Qte", "Taxes", "PrixHT", "PrixTTC", "actions"];
  dataSource = new MatTableDataSource<PanierRow>();

  constructor(private panierService: PanierService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.panierService.panier$.subscribe(panier => {
      this.dataSource.data = panier.rows;
    });
  }

  supprimerArticle(article: Article) {
    this.panierService.supprimerArticle(article);
    this._snackBar.open( article.productName + " a été supprimé avec succès de votre panier");
  }

  calculeTotaleTaxes() {
    return this.dataSource.data
      .map(row => row.article.taxes * row.qte)
      .reduce((sum, value) => sum + value, 0);
  }

  calculeTotaleTTC() {
    return this.dataSource.data
      .map(row => row.article.prixUnitaireTTC  * row.qte)
      .reduce((sum, value) => sum + value, 0);
  }
}
