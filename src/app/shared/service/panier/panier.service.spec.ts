import { TestBed } from '@angular/core/testing';

import { PanierService } from './panier.service';
import {BehaviorSubject} from "rxjs";
import {Panier} from "../../model/panier";
import {Article} from "../../model/article";

describe('PanierService', () => {
  let service: PanierService;
  let panierSubject: BehaviorSubject<Panier>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PanierService]
    });
    service = TestBed.inject(PanierService);
    // Get the subject to manually emit values
    panierSubject = (service as any)._panierSubject;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add article to panier', () => {
    const article: Article = { id: 1, productName: 'Article 1' } as Article;
    const qte = 2;

    service.ajouterArticle(article, qte);

    // Expect panier to have the new article
    expect(service.getRowArticle(article)).toBeTruthy();
  });

  it('should increment quantity of existing article in panier', () => {
    const article: Article = { id: 1, productName: 'Article 1' } as Article;
    const qte = 2;

    service.ajouterArticle(article, qte);

    // Expect quantity to be incremented
    const row = service.getRowArticle(article);
    expect(row?.qte).toBe(qte);
  });

  it('should delete article from panier', () => {
    const article: Article = { id: 1, productName: 'Article 1' } as Article;
    const qte = 2;

    service.ajouterArticle(article, qte);
    service.supprimerArticle(article);

    // Expect panier to not have the deleted article
    expect(service.getRowArticle(article)).toBeDefined();
  });

  it('should decrement quantity of existing article in panier', () => {
    const article: Article = { id: 1, productName: 'Article 1' } as Article;
    const qte1 = 2;
    const qte2 = 1;

    service.ajouterArticle(article, qte1);
    service.ajouterArticle(article, qte2);
    service.supprimerArticle(article);

    // Expect quantity to be decremented
    const row = service.getRowArticle(article);
    expect(row?.qte).toBe(qte1 - qte2);
  });

  it('should emit panier update after operations', () => {
    const article: Article = { id: 1, productName: 'Article 1' } as Article;
    const qte = 2;

    // Spy on next method of panierSubject
    spyOn(panierSubject, 'next');

    service.ajouterArticle(article, qte);

    // Expect panierSubject.next to have been called after adding article
    expect(panierSubject.next).toHaveBeenCalled();
  });
});
