import { Injectable } from '@angular/core';
import {Article} from "../../model/article";
import {Panier, PanierRow} from "../../model/panier";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  private _panier: Panier = new Panier();

  private _panierSubject: BehaviorSubject<Panier> = new BehaviorSubject<Panier>(this._panier);

  panier$: Observable<Panier> = this._panierSubject.asObservable();

  ajouterArticle(article: Article, qte: number): PanierRow {
    let row = this._panier.getRow(article.id);
    if(row) {
      row.incrementQte(qte);
    } else {
      row = this._panier.ajouterRow(article,qte);
    }

    this._panierSubject.next(this._panier);

    return row;
  }

  getRowArticle(article: Article): PanierRow | undefined {
    return this._panier.getRow(article.id);
  }
  supprimerArticle(article: Article) {
    const row = this._panier.getRow(article.id);
    if(row) {
      this._panier.decrementQteArticle(article)
    }
    this._panierSubject.next(this._panier);
  }
}
