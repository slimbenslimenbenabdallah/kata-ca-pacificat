import {Component, OnInit} from '@angular/core';
import {PanierService} from "../../service/panier/panier.service";
import {Observable} from "rxjs";
import {Panier} from "../../model/panier";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit{

  panier$: Observable<Panier>;

  constructor(private panierService: PanierService) {

  }

  ngOnInit(): void {
    this.panier$ = this.panierService.panier$;
  }

}
