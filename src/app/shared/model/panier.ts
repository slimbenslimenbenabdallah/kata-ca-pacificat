import {Article} from "./article";

export class Panier {
  private _rows: PanierRow[] = [];

  get rows(): PanierRow[] {
    return this._rows;
  }

  get totalArticle(): number {
    return this._rows
      .map(row => row.qte)
      .reduce((sum, qte) => sum + qte, 0);
  }

  ajouterRow = (article: Article, qte: number): PanierRow => {
    const panierRow = new PanierRow(article, qte);
    this._rows.push(panierRow);
    return panierRow
  }

  decrementQteArticle = (article: Article): void => {
    const index = this._rows.findIndex(row => row.article.id === article.id);
    let row = this._rows[index];
    if (row) {
      row.decrementQte();
      if (row.qte === 0) {
        this._rows.splice(index, 1);
      }
    }
  }

  getRow = (articleId: number): PanierRow | undefined => this._rows.find(row => row.article.id === articleId);

}

export class PanierRow {
  constructor(public article: Article,
              public qte: number) {
  }

  incrementQte = (qte: number): void => {
    if (this.qte < this.article.quantity) {
      this.qte+=qte;
    }
  }

  decrementQte = (): void => {
    if (this.qte) {
      this.qte--;
    }
  }
}
