import {Panier} from "./panier";
import {Article} from "./article";


describe('Panier', () => {
  let article: Article;
  let panier: Panier;

  beforeEach(() => {
    panier = new Panier();
    article = {
      "id": 1,
      "productName": "test",
      "price": 1.76,
      "quantity": 7,
      "isImported": true,
      "category": "Food"
    } as Article;
  });

  it('should be able to add a row', () => {
    panier.ajouterRow(article, 2);
    expect(panier.rows.length).toBe(1);
  });

  it('should decrement quantity of an article', () => {
    panier.ajouterRow(article, 3);
    panier.decrementQteArticle(article);
    expect(panier.getRow(article.id)?.qte).toBe(2);
  });

  it('should remove row when quantity is decremented to zero', () => {
    panier.ajouterRow(article, 1);
    panier.decrementQteArticle(article);
    expect(panier.getRow(article.id)).toBeUndefined();
  });

  it('should increment quantity of an article', () => {
    panier.ajouterRow(article, 3);
    const row = panier.getRow(article.id);
    if (row) {
      row.incrementQte(2);
      expect(row.qte).toBe(5);
    }
  });

  it('should calculate total article quantity', () => {
    const article2: Article = {
      "id": 2,
      "productName": "test",
      "price": 1.76,
      "quantity": 7,
      "isImported": true,
      "category": "Food"
    } as Article;
    panier.ajouterRow(article, 2);
    panier.ajouterRow(article2, 1);
    expect(panier.totalArticle).toBe(3);
  });
});
