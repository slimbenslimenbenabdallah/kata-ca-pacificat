import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundNearest5Centime'
})
export class RoundNearest5CentimePipe implements PipeTransform {

  transform(value: number): number {
    if(!value) {
      value = 0;
    }
    return +Number(+value.toFixed(2) - (value % 0.05)).toFixed(2);
  }

}
