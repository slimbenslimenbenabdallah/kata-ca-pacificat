import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: number, fractionDigits: number = 2): number {
    return +value.toFixed(fractionDigits);
  }

}
