import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './component/header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatBadgeModule} from "@angular/material/badge";
import {RouterLink} from "@angular/router";
import {RoundNearest5CentimePipe} from "./pipe/round-nearest-5-centime/round-nearest-5-centime.pipe";
import { FormatNumberPipe } from './pipe/fomat-number/format-number.pipe';


@NgModule({
  declarations: [
    HeaderComponent,
    RoundNearest5CentimePipe,
    FormatNumberPipe
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatBadgeModule,
    RouterLink
  ],
  exports: [HeaderComponent, RoundNearest5CentimePipe, FormatNumberPipe]
})
export class SharedModule {
}
